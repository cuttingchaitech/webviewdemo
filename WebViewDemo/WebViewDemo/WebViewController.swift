//
//  WebViewController.swift
//  WebViewDemo
//
//  Created by Salman Qureshi on 7/31/20.
//  Copyright © 2020 Salman Qureshi. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    var webView: WKWebView!
    var newString: String?
    var lat: Double = 0.0
    var lon: Double = 0.0
    
    var myUrl: URL?
    var fromDetailScreen: Bool = false
    
    var finalUrl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if fromDetailScreen && finalUrl != nil {
             myUrl = URL(string: finalUrl!)
        } else {
            myUrl = URL(string:"https://pwa.cuttingchaitech.in")
        }
        
        let myRequest = URLRequest(url: myUrl!)
        print("Latitude: \(lat), Longitude: \(lon)")
        webView.load(myRequest)
        
        
        
    }
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.userContentController.add(self, name: "scanButtonClicked")
        
        
        // Get script that's to be injected into the document
        let js:String = "onClickScanEnter"
        
        // Specify when and where and what user script needs to be injected into the web document
        let userScript:WKUserScript =  WKUserScript(source: js,
                                                    injectionTime: WKUserScriptInjectionTime.atDocumentEnd,
                                                    forMainFrameOnly: true)
        
        webConfiguration.userContentController.addUserScript(userScript)
        webConfiguration.preferences.javaScriptCanOpenWindowsAutomatically = true
        webConfiguration.preferences.javaScriptEnabled = true
        webConfiguration.defaultWebpagePreferences.preferredContentMode = .mobile
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.allowsLinkPreview = true
        webView.uiDelegate = self
        view = webView
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WebViewController: WKUIDelegate, WKScriptMessageHandler {
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        if message.name == "scanButtonClicked" {
            if let messageBody: NSDictionary = message.body as? NSDictionary{
                // Do stuff with messageBody
                stopLoadingWebView(token: messageBody.value(forKey: "token") as! String)
            }
        }
    }
    
    func stopLoadingWebView(token: String) {
        self.navigationController?.popViewController(animated: true)
        moveToScanVC(token: token)
    }
    
    func moveToScanVC(token: String) {
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainSB.instantiateViewController(withIdentifier: "QRScannerViewController") as! QRScannerViewController
        vc.tokenString = token
        print("\(vc.tokenString)")
        self.navigationController?.setViewControllers([vc], animated: true)
    }
}
