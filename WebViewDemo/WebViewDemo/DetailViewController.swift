
import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailLabel: CopyLabel!
    
    var qrData: QRData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailLabel.text = qrData?.codeString
        UIPasteboard.general.string = detailLabel.text
        showToast(message : "Text copied to clipboard")

    }

    @IBAction func openInWebAction(_ sender: Any) {
        if let url = URL(string: qrData?.codeString ?? "") {
          //  UIApplication.shared.open(url)
           
            let mainSB = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainSB.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            print("\(url)")
            vc.fromDetailScreen = true
            vc.finalUrl = qrData?.codeString ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            //        } else {
//            showToast(message : "Not a valid URL")
        }
    }
    
    func openWebAction(_ qrValue: String) {
        if let url = URL(string: qrValue) {
            
            let mainSB = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainSB.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            print("\(url)")
            vc.fromDetailScreen = true
            vc.finalUrl = qrData?.codeString ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            //        } else {
            //            showToast(message : "Not a valid URL")
        }
    }
}
