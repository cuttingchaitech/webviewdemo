//
//  ViewController.swift
//  WebViewDemo
//
//  Created by Salman Qureshi on 7/31/20.
//  Copyright © 2020 Salman Qureshi. All rights reserved.
//

import UIKit
import Foundation
import SafariServices
import CoreLocation

class ViewController: UIViewController {
    
    let locationManager = CLLocationManager()
    var newLocation: CLLocation?
    var sjsScript = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        locationManager.requestAlwaysAuthorization()
             locationManager.allowsBackgroundLocationUpdates = true
             locationManager.pausesLocationUpdatesAutomatically = false
             locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        
       presentCamera()
        
    }
    
    func presentCamera() {
        let photoPicker = UIImagePickerController()
        photoPicker.sourceType = .camera
       // photoPicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
    
        self.present(photoPicker, animated: true, completion: nil)
    }

    @IBAction func openSafariWebLinkButtton(_ sender: UIButton) {
        guard let url = URL(string: "https://pwa.cuttingchaitech.in") else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            // Fallback on earlier versions
            let svc = SFSafariViewController(url: url)
            present(svc, animated: true, completion: nil)
        }
    }

    @IBAction func openWkWebViewButton(_ sender: UIButton) {
        let controller = WebViewController()
        
        if newLocation != nil {
            let epoch = Int(newLocation!.timestamp.timeIntervalSince1970)
            let course: Float = -1.0
            let speed: Float = -1.0
            let coords = String(
                format: "coords: { latitude: %f, longitude: %f, altitude: %f, heading: %f, speed: %f, accuracy: %f, altitudeAccuracy: %f }", newLocation!.coordinate.latitude, newLocation!.coordinate.longitude, newLocation!.altitude, course, speed, newLocation!.horizontalAccuracy, newLocation!.verticalAccuracy)

            let jsCallBack = String(format: "navigator.geolocation.setLocation({ timestamp: %d, %@ });", epoch, coords)
            controller.newString = jsCallBack
            controller.lat = Double(newLocation!.coordinate.latitude)
            controller.lon = Double(newLocation!.coordinate.longitude)
            navigationController?.pushViewController(controller, animated: true)
        } else {
            self.view.makeToast("Location not provided")
        }
        
       
    }
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let lastLocation = locations.last else {
            return
        }
        
        newLocation = lastLocation
        print(newLocation)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("didFail error; \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedAlways  {
            
            //Inventa.shared.discoveryDelegate = self
            //Inventa.shared.allowLocationPermission = true
            locationManager.startUpdatingLocation()
            //Inventa.shared.monitorGeoZones()
            print("Location permission granted \(status.rawValue)")
        } else if status == .authorizedWhenInUse {
            locationManager.startMonitoringSignificantLocationChanges()
          // Inventa.shared.allowLocationPermission = true
        } else {
            // Inventa.shared.allowLocationPermission = false
             print("Location permission granted \(status.rawValue)")
        }
        
    }
}
